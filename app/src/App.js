import React from 'react';
import { Provider } from 'react-redux'
import store from './redux/store'
import 'reset-css'
import './App.scss';
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'
import Nav from './components/Nav';
const options = {
  position: positions.MIDDLE,
  offset: '30px',
  transition: transitions.FADE
}
function App() {
  return (
    <Provider store={store}>
      <AlertProvider template={AlertTemplate} {...options}>
        <Nav />
      </AlertProvider>
    </Provider>
  );
}

export default App;
