import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Reading from './reading/Reading'
import Output from './output/Output'

export default function Nav() {
    return (
        <Router>
                <nav>
                    <ul className='nav'>
                        <li>
                            <Link to="/">読み込み</Link>
                        </li>
                        <li>
                            <Link to="/output">出力</Link>
                        </li>
                    </ul>
                </nav>
                <Switch>
                    <Route path="/" exact>
                        <Reading />
                    </Route>
                    <Route path="/output">
                        <Output />
                    </Route>
                </Switch>
        </Router>
    )
}
