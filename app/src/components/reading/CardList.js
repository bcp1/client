import React from 'react'
import { useSelector } from 'react-redux'
import Card from './Card'

function CardList() {
    const imageList = useSelector(state => state.card.imageList)
    const isImageVisible = useSelector(state => state.card.isImageVisible)

    function renderImageList(){
        let images = []
        for (let index = 0; index < imageList.length; index++) {
            const el = imageList[index];
            images.push(<Card key={el.id} id={el.id} name={el.name} file={el.file} isShow={el.isShow} />)
        }
        return images
    }

    return (
        <ul>
            {isImageVisible && renderImageList()}
        </ul>
    )
}

export default CardList
