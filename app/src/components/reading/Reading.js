import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import './reading.scss';
import BtnChooseCard from './BtnChooseCard'
import BtnReading from './BtnReading'
import CardList from './CardList'
import { deleteAllCard } from '../../redux'

export default function Reading() {
    const dispatch = useDispatch()

    useEffect(() => {
        return () => {
            dispatch(deleteAllCard())
        };
    })

    return (
        <div　className='main'>
            <div className='reading-btn-list'>
                <BtnChooseCard />
                <BtnReading />
            </div>
            <CardList />
        </div>
    )
}
