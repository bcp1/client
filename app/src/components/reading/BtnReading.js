import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import axios from 'axios';
import { useAlert } from 'react-alert'
import { deleteAllCard } from '../../redux'

const API_ENDPOINT = 'http://localhost:5000/cards/reading';

function BtnReading() {
    const imageList = useSelector(state => state.card.imageList)
    const dispatch = useDispatch()
    const alert = useAlert()

    async function postImage(imageList) {
        try {
            let json = []
            for (let i = 0; i < imageList.length; i++) {
                json.push({
                    name: imageList[i].name,
                    image: imageList[i].file,
                })
            }
            let result = await axios.post(API_ENDPOINT, json)
            return result
        } catch (error) {
            console.log(error.response)
            console.log(`Error! HTTP`)
            return error.response
        }
    }

    function handleOnClick() {
        if (imageList.length === 0) {
            alert.error('画像を選択して下さい。')
            return 0
        }

        postImage(imageList)
            .then(result => {
                dispatch(deleteAllCard())
                console.log(result)
                if (result.status === 200) {
                    alert.show('送信が完了しました。')
                }else if(result.status === 500){
                    alert.error('エラーが発生しました。')
                }
            }
            )
    }
    return (
        <button className='btn-reading' onClick={handleOnClick}>
            送信
        </button>
    )
}

export default BtnReading