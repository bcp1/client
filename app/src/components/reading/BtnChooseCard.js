import React from 'react'
import { useDispatch } from 'react-redux'
import { addCard, deleteAllCard, showCard, deleteCardInfo } from '../../redux'

function BtnChooseCard() {
    const dispatch = useDispatch()

    function getBase64(file, loop, cb) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
            loop()
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }

    function handleChangeFile(e) {
        dispatch(deleteAllCard())
        dispatch(deleteCardInfo())
        let files = e.target.files;
        let Loop = loop(files.length);
        for (let i = 0; i < files.length; i++) {
            getBase64(files[i], Loop, (file) => {
                dispatch(addCard({ 'id': i, 'name': files[i].name, 'file': file , 'isShow': false}))
            })
        }

    }

    let loop = (max) => {
        let count = 0;
        return () => {
            count++;
            if (count >= max) {
                dispatch(showCard())
            }
        };
    };

    return (
        <label htmlFor={'cards-input'} className='cards-input-label'>
            <span className='cards-input-name'>選択</span>
            <input id={'cards-input'} type="file" multiple="multiple"
                className='cards-input'
                onChange={handleChangeFile}
                onClick={(e) => { e.target.value = '' }} />
        </label>
    )
}

export default BtnChooseCard