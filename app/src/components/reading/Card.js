import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import ModalImage from "react-modal-image";
import Modal from 'react-modal';
import Box from '@material-ui/core/Box';
import { green } from '@material-ui/core/colors';
import { IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { deleteCard } from '../../redux'

Modal.setAppElement('#root')
export default function Card(props) {
    const dispatch = useDispatch()
    const info = useSelector(state => state.card.info)

    const [modalIsOpen, setIsOpen] = useState(false)

    function make_data_list(id) {
        let data_list = []
        for (let i = 0; i < info.length; i++) {
            if (info[i].id === id) {
                let data = info[i].data
                for (let index = 0; index < data.length; index++) {
                    data_list.push(<li key={index}>{data[index][0]} : {data[index][1]}</li>)
                }
            }
        }
        return data_list
    }

    function make_data_text_field(id) {
        let data_list = []
        for (let i = 0; i < info.length; i++) {
            if (info[i].id === id) {
                let data = info[i].data
                for (let index = 0; index < data.length; index++) {
                    data_list.push(<div key={index}>
                        <input className='modal-card-info-title' type='text' defaultValue={data[index][0]}></input>
                        <input className='modal-card-info-detail' type='text' defaultValue={data[index][1]}></input>
                    </div>)
                }
            }

        }
        return data_list
    }

    return (
        <li className='card-panel'>
            <label className='card-btn-delete'>
                <IconButton color='secondary' onClick={() => dispatch(deleteCard(props.id))}>
                    <DeleteIcon />
                </IconButton>
            </label>
            <span className='card-name'>{props.name}</span>
            <ModalImage
                className='card'
                small={props.file}
                large={props.file}
                alt={`${props.name}`}
                hideDownload={true}
                hideZoom={true}
            />
            {props.isShow &&
                <div>
                    <IconButton onClick={() => setIsOpen(true)} style={{ color: green[500] }} className='card-btn-edit'>
                        <EditIcon /> Edit
                    </IconButton>
                    <ul className='card-info'>
                        {make_data_list(props.id)}
                    </ul>
                    <Modal
                        isOpen={modalIsOpen}
                        onRequestClose={() => setIsOpen(false)}
                        contentLabel="Example Modal">
                        <h2>{props.name}</h2>
                        <Box display="flex" >
                            <img className='modal-card-image' src={props.file} alt={props.name} />
                            <div className='modal-card-info-panel'>
                                {make_data_text_field(props.id)}
                            </div>
                        </Box>
                        <button className='modal-btn-close' onClick={() => setIsOpen(false)}>close</button>
                    </Modal>
                </div>
            }
        </li>
    )
}