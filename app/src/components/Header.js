import React, { Component } from 'react'

export default class Header extends Component {
    render() {
        return (
            <header className='header'>
                <h1>Business Card Reading App</h1>
            </header>
        )
    }
}
