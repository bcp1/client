import {
  ADD_CARD,
  UPDATE_CARD,
  DELETE_CARD,
  DELETE_ALL_CARD,
  SHOW_CARD,
  HIDE_CARD,
  ADD_CARD_INFO,
  DELETE_CARD_INFO,
  SHOW_CARD_INFO,
  HIDE_CARD_INFO
} from './cardTypes'

export const addCard = (image) => {
  return {
    type: ADD_CARD,
    payload: image
  }
}

export const deleteCard = (id) => {
  return {
    type: DELETE_CARD,
    payload: id
  }
}

export const updateCard = (image) => {
  return {
    type: UPDATE_CARD,
    payload: image
  }
}

export const deleteAllCard = () => {
  return {
    type: DELETE_ALL_CARD
  }
}

export const showCard = () => {
  return {
    type: SHOW_CARD
  }
}

export const hideCard = () => {
  return {
    type: HIDE_CARD
  }
}

export const addCardInfo = (info) => {
  return {
    type: ADD_CARD_INFO,
    payload: info
  }
}

export const deleteCardInfo = () => {
  return {
    type: DELETE_CARD_INFO
  }
}

export const showCardInfo = () => {
  return {
    type: SHOW_CARD_INFO
  }
}

export const hideCardInfo = () => {
  return {
    type: HIDE_CARD_INFO
  }
}