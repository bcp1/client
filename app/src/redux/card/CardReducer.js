import {
    ADD_CARD,
    UPDATE_CARD,
    DELETE_CARD,
    DELETE_ALL_CARD,
    SHOW_CARD,
    HIDE_CARD,
    ADD_CARD_INFO,
    DELETE_CARD_INFO,
    SHOW_CARD_INFO,
    HIDE_CARD_INFO
} from './cardTypes'

const initialState = {
    imageList: [],
    isImageVisible: false,
    info: [],
    isInfoVisible: false
}

const cardReducer = (state = initialState, action) => {
    switch (action.type) {

        case ADD_CARD:
            return {
                ...state,
                imageList: [...state.imageList, action.payload]
            }

        case UPDATE_CARD:
            return {
                ...state,
                imageList: state.imageList.map(el => el.id === action.payload.id ? action.payload : el)
            }

        case DELETE_CARD:
            return {
                ...state,
                imageList: state.imageList.filter((item) => item.id !== action.payload)
            }

        case DELETE_ALL_CARD:
            return {
                ...state,
                imageList: []
            }

        case SHOW_CARD:
            return {
                ...state,
                isImageVisible: true
            }

        case HIDE_CARD:
            return {
                ...state,
                isImageVisible: false
            }

        case ADD_CARD_INFO:
            return {
                ...state,
                info: [...state.info, action.payload]
            }
        case DELETE_CARD_INFO:
            return {
                ...state,
                info: []
            }

        case SHOW_CARD_INFO:
            return {
                ...state,
                isInfoVisible: true
            }

        case HIDE_CARD_INFO:
            return {
                ...state,
                isInfoVisible: false
            }

        default: return state
    }
}

export default cardReducer