import { combineReducers } from 'redux'
import cardReducer from './card/CardReducer'

const rootReducer = combineReducers({
    card: cardReducer,
})

export default rootReducer